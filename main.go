package main

import (
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"encoding/json"
)

type Response struct {
	RemoteSocket   string `json:"remoteSocket"`
	LocalSocket    string `json:"localSocket"`
	Data       []byte `json:"data"`
}

func main() {
	waitGroup := sync.WaitGroup{}
	listener, err := net.Listen("tcp", ":7777")
	if err != nil {
		log.Fatal("Listener failed:", err)
	}
	defer listener.Close()
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	keepRunning := true

	go func() {
		<-interrupt
		log.Println("Wrapping up")
		keepRunning = false
		listener.Close()
	}()

	for keepRunning {
		conn, err := listener.Accept()
		if err != nil && keepRunning {
			log.Fatal("Accept failed:", err)
		}
		go func(c net.Conn) {
			defer c.Close()
			defer waitGroup.Done()
			waitGroup.Add(1)
			in, err := ioutil.ReadAll(c)
			if err != nil {
				log.Println("Couldn't read data:", err)
				return
			}
			response := Response{
				RemoteSocket: c.RemoteAddr().Network() + "://" + c.RemoteAddr().String(),
				LocalSocket: c.LocalAddr().Network() + "://" + c.LocalAddr().String(),
				Data: in,
			}

			out, err := json.Marshal(response)
			if err != nil {
				log.Println("Couldn't marshal into json:", err)
				return
			}

			n, err := conn.Write(out)
			if err != nil {
				log.Println("Couldn't write to socket:", err)
				return
			}
			if n != len(out) {
				log.Println("Partial response written")
			}
		}(conn)
	}
	log.Println("Waiting...")
	waitGroup.Wait()
}

